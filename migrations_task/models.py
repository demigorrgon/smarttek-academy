from django.db import models


class SomeVeryCoolAndInformativeModel(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birthday = models.CharField(max_length=50)

    def full_name(self):
        return f"{self.first_name} {self.last_name}"
