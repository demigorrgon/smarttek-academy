from django.apps import AppConfig


class MigrationsTaskConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'migrations_task'
